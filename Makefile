call:: _clean all
all:: public/site.css
	./bottle -b:80 ws:app --debug --reload -sgevent
public/site.css:: tailwind.css
	tailwindcss -i tailwind.css -o public/site.css -m
watch::
	tailwindcss -i tailwind.css -o public/site.css -mw
_clean:: clean tree
clean::
	find . -name '*~' -o -name '.*~' | xargs rm -fr
	rm -fr __pycache__ node_modules dist .parcel-cache
	rm -fr yarn.lock bun.lockb package-lock.json
	rm -fr public/site.css
tree:;	tree -a -I .git
