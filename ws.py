from bottle import *;app=app()
def set_defaults(ret=None):
    headers=(ret or response).headers
    headers['Content-Language'] = 'en-US'
    headers['Cache-Control'] = 'no-store, must-revalidate'
    headers['Expires'] = '0'
    return ret
role='public'
@get('/')
@get('<path:path>/')
@get('<path:path><ext:re:\\.html>')
def _(path='',ext='/index.html'):
    set_defaults()
    d = dict()
    return jinja2_template(role+path+ext,d)
@get('<path:path>')
def _(path):
    return set_defaults(static_file(role+path,'.'))
