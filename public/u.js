addEventListener('click',async(e,D,t,a,b)=>{
    D=document,t=e.target
    if(a=t.getAttribute('@@')){
	if(b=t.getAttribute('href')){
	    e.preventDefault()
	    t=await fetch(b)
	    D.querySelector(a).innerHTML=t=await t.text()
	    history.replaceState({}, D.title, b)
	}
    }
    if(a=t.getAttribute('@')){
	if(b=t.getAttribute('href')){
	    e.preventDefault()
	    t=await fetch('_'+b)
	    D.querySelector(a).innerHTML=t=await t.text()
	    history.replaceState({}, D.title, b)
	}
    }
})
